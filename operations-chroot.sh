#! /bin/bash

version="PrimTux4 Lubuntu 18.04 amd64 CTP"

# Indiquer ici le nombre de tentatives d'installation à faire automatiquement dansle cas où des installations ont échoué
tentatives=3

# Variables contenant les paquets à installer ; c'est ici qu'il faudra en modifier les listes
paquets="gksu python-imaging base-files user-setup desktop-base coreutils dash e2fsprogs gzip hostname init login systemd gnupg locales console-setup lintian acpi x11-utils x11-apps accountsservice sudo gksu xdg-utils anacron bash bash-completion mesa-utils x11-common x11-session-utils x11-xkb-utils x11-xserver-utils xapps-common xinit grub-pc xorg xserver-xorg-video-all xserver-xorg-input-all xserver-common xserver-xorg-input-mouse xserver-xorg-input-wacom xserver-xorg-video-mach64 xserver-xorg-video-qxl xserver-xorg-video-r128 xserver-xorg-video-radeon xserver-xorg-video-sisusb xserver-xorg-video-tdfx vdpau-va-driver i965-va-driver xserver-xorg-video-intel xserver-xorg-video-vesa xserver-xorg-video-qxl xserver-xorg-video-nouveau xserver-xorg-video-fbdev alsa-utils alsa-tools sox pulseaudio gspeech flac wavpack lame libasound2-plugins python-feedparser python-gpod sox mpg321 gstreamer1.0-plugins-bad gstreamer1.0-plugins-base gstreamer1.0-packagekit gstreamer1.0-plugins-good gstreamer1.0-plugins-ugly gstreamer1.0-x gstreamer1.0-fluendo-mp3 gstreamer1.0-alsa gstreamer1.0-libav libavformat57 libavcodec-extra57 libphonon4 libpulse0 libpulse-mainloop-glib0 frei0r-plugins alsa-firmware-loaders python-apt xdg-user-dirs usbutils libmtp-runtime jmtpfs mtp-tools cpulimit trash-cli xserver-xorg-input-synaptics gxmessage fluxbox cdparanoia aspell aspell-fr baobab dillo itk3 iwidgets4 libstdc++5 ntfs-3g zenity unzip numlockx eject gnucap mlocate pavucontrol aptitude dkms gcc apt-transport-https libappindicator3-1 whois python-libuser python-glade2 vim udevil tcl8.5 tk8.5 tcl tk libtk-img python-qt4 python-sip udevil at-spi2-core tix hunspell-fr gtkdialog libssl0.9.8 libssl1.0.0 libt1-5 libttspico-data libttspico-utils libttspico0 lightdm-webkit-greeter tix at-spi2-core lightdm lightdm-gtk-greeter gnome-keyring libpam-gnome-keyring policykit-1-gnome pam-dbus-notify gnome-system-tools pinta mirage imagemagick gimp gimp-plugin-registry xsane zlib1g libx11-6 e2fsprogs gparted file-roller fusesmb gvfs gvfs-bin gvfs-fuse gvfs-backends samba gigolo wicd iproute2 iptables net-tools dnsutils firefox firefox-locale-fr ntp debconf dnsmasq gamin lighttpd lighttpd-mod-magnet perl php-cgi php-xml libnotify-bin notification-daemon iptables-persistent rsyslog e2guardian privoxy openssl libnss3-tools console-data dnsutils whiptail ifupdown rsync openssh-server cups printer-driver-cups-pdf hplip hplip-gui hpijs-ppds printer-driver-c2050 printer-driver-c2esp printer-driver-cjet printer-driver-escpr openprinting-ppds gutenprint-locales printer-driver-gutenprint printer-driver-hpcups printer-driver-postscript-hp printer-driver-m2300w printer-driver-min12xxw printer-driver-pnm2ppa printer-driver-ptouch printer-driver-sag-gdi printer-driver-splix printer-driver-foo2zjs printer-driver-hpijs printer-driver-pxljr system-config-printer magicfilter djtools librecode0 recode lpr xfonts-100dpi xfonts-75dpi xfonts-base xfonts-terminus fonts-liberation gnome-font-viewer fonts-opendyslexic fonts-dejavu-core fonts-dejavu-extra fonts-liberation fonts-sil-gentium fonts-sil-gentium-basic gnome-icon-theme-symbolic ttf-dejavu-core gtk2-engines-murrine murrine-themes adwaita-icon-theme faenza-icon-theme gtk2-engines gtk2-engines-pixbuf mothsart-wallpapers-primtux arandr menulibre pcmanfm spacefm rox-filer leafpad shutter xfburn gnome-calculator xournal libreoffice libreoffice-help-fr libreoffice-l10n-fr libreoffice-pdfimport osmo xpdf filezilla vlc openshot winff audacity soundconverter htop hardinfo xscreensaver seahorse gnome-packagekit onboard krita xpaint krita-l10n tuxguitar musescore geany accueil-primtux2 administration-eleves-primtux arreter-primtux-ubuntu autologin-primtux-ubuntu fskbsetting roxterm-common roxterm-gtk2 synapse toutenclic lxpanel lxappearance xfce4-panel xfce4-cpufreq-plugin xfce4-datetime-plugin xfce4-fsguard-plugin xfce4-indicator-plugin xfce4-pulseaudio-plugin xfce4-netload-plugin xfce4-quicklauncher-plugin xfce4-systemload-plugin xfce4-notifyd synaptic gdebi lsb-release gcompris-qt-primtux pysycache pysycache-buttons-beerabbit pysycache-buttons-crapaud pysycache-buttons-ice pysycache-buttons-wolf pysycache-click-dinosaurs pysycache-click-sea pysycache-dblclick-appleandpear pysycache-dblclick-butterfly pysycache-i18n pysycache-images pysycache-move-animals pysycache-move-food pysycache-move-plants pysycache-move-sky pysycache-move-sports pysycache-puzzle-cartoons pysycache-puzzle-photos pysycache-sounds tuxpaint childsplay childsplay-alphabet-sounds-fr childsplay-alphabet-sounds-ca ri-li supertux hannah tuxmath gtans stellarium seahorse-adventures scratch ktuberling klettres kde-l10n-fr goldendict tuxpaint-config blobby frozen-bubble jclic jclic-reports tbo monsterz leterrier-aller drgeo-primtux edit-interactive-svg geonext-primtux geotortue-stretch intef-exe jnavigue-primtux microscope-virtual-primtux multiplication-station-primtux omega-primtux omnitux-light openboard pylote-primtux pysiogame qdictionnaire leterrier-cibler leterrier-fubuki leterrier-suitearithmetique leterrier-tierce etoys firmware-b43-installer firmware-b43legacy-installer b43-fwcutter alsa-firmware-loaders iucode-tool intel-microcode"

# Liste des paquets i386 à installer
paquets386="libqt4-svg:i386 libgl-gst:i386 libxrandr2:i386 libxcursor1:i386 libxinerama1:i386 libxft2:i386 libstdc++5:i386 libqt4-sql-mysql:i386 libcairo2:i386 libxmu6:i386 gtk2-engines:i386 libgtk2.0-0:i386 libgdk-pixbuf2.0-0:i386 wine32:i386 libsqlite3-0:i386 libqt4-network:i386 libqt4-script:i386 libqt4-xml:i386 libudev1:i386 libexif12:i386 libnss3:i386 libgconf2-4:i386 libxss1:i386 leterrier-calculette-capricieuse:i386 leterrier-calcul-mental:i386 leterrier-imageo:i386 handymenu"

# Paquets locaux à installer
paquets_locaux="/usr/share/primtux/system-config-samba_1.2.63-0ubuntu6_all.deb"

# Pour établir la liste des paquets non installés
fic_liste_manquants="/tmp/liste-paquets-manquants.txt"
liste_paquets=$(echo "$paquets $paquets386" | sed 's/ /\n/g')
liste_locaux=$(echo "$paquets_locaux" | sed 's/ /\n/g' | sed -e 's/.*\///' -e 's/_.*\.deb//g')

manquants=""
installes="/tmp/paquets-installes.txt"
erreurs="non"
etablit_manquants () {
  dpkg -l | sed -e '1,6d' -e "s/[ ][ ]*/#/g" | cut -d '#' -f 2 > "$installes"
  manquants=""
  erreurs="non"
  while read paquet
    do
    if ! grep "$paquet" "$installes" > /dev/null
      then manquants="$manquants $paquet"
          erreurs="oui"
    fi
  done <<<"$liste_paquets"
  manquants=$(echo "$manquants" | tr " " "\n" | sort -u | tr "\n" " " | sed 's/^ //')
}

# Redirection d'erreurs vers un fichier temporaire
fic_log="/tmp/operations-chroot.log"
> "$fic_log"
exec 2>>"$fic_log"

# Ajout des dépôts
apt-get update
add-apt-repository --yes ppa:jclic/master
add-apt-repository --yes ppa:webupd8team/java
wget -O – https://depot.primtux.fr/repo/debs/key/PrimTux.gpg.key | apt-key add –
wget -q – https://depot.primtux.fr/repo/debs/key/PrimTux.gpg.key | apt-key add –
rm PrimTux.gpg.key –
echo "deb https://depot.primtux.fr/repo/debs PrimTux2-Stretch64 main
deb https://depot.primtux.fr/repo/debs PrimTux4-amd64 main" >/etc/apt/sources.list.d/primtux.list
echo "deb http://archive.ubuntu.com/ubuntu/ artful universe" >/etc/apt/sources.list.d/artful.list
apt-get update

# Reconfigure les locales
update-locale "LANG=fr_FR.UTF-8"
locale-gen --purge "fr_FR.UTF-8"
dpkg-reconfigure --frontend noninteractive locales

## Installation des paquets nécessaires à PrimTux
apt-get update && apt-get dist-upgrade -y
wget -c -nv https://primtux.fr/sources-lubuntu-18.04/libhunspell-1.4-0_1.4.1-2build1_amd64.deb
dpkg -i libhunspell-1.4-0_1.4.1-2build1_amd64.deb
rm libhunspell-1.4-0_1.4.1-2build1_amd64.deb

# Lance l'installation
for paquet in ${paquets}
do
  apt-get install -y --no-install-recommends "$paquet"
done

# Nettoyage
apt-get autoremove -y fonts-noto-cjk abiword-common gnumeric
dpkg --purge $(COLUMNS=200 dpkg -l | grep "^rc" | tr -s ' ' | cut -d ' ' -f 2)

## Paramétrages de PrimTux ##
wget -c -nv https://primtux.fr/sources-lubuntu-18.04/includes.chroot.tar.gz
tar xvf includes.chroot.tar.gz -C /tmp
rsync -av /tmp/includes.chroot/* /
rm includes.chroot.tar.gz
rm -rf /tmp/includes.chroot

# Efface les utilisateurs s'ils existent déjà
if [ -d /home/01-mini ]
  then userdel 01-mini
  rm -rf /home/01-mini # Dans le cas où le répertoire n'appartient pas à son propriétaire, userdel -r ne le supprime pas
fi
if [ -d /home/02-super ]
  then userdel 02-super
  rm -rf /home/02-super
fi
if [ -d /home/03-maxi ]
  then userdel 03-maxi
  rm -rf /home/03-maxi
fi
if [ -d /home/administrateur ]
  then userdel administrateur
  rm -rf /home/administrateur
fi
# Ajout des utilisateurs:
useradd --password oymab1U7DSUmg -m -k /etc/skel administrateur
useradd --password oymab1U7DSUmg -m -k /etc/skel-mini 01-mini
useradd --password oymab1U7DSUmg -m -k /etc/skel-super 02-super
useradd --password oymab1U7DSUmg -m -k /etc/skel-maxi 03-maxi
usermod --password oymab1U7DSUmg root

# Hooks login et sécurité :
groupadd -r nopasswdlogin
gpasswd -a 01-mini nopasswdlogin
gpasswd -a 02-super nopasswdlogin
gpasswd -a 03-maxi nopasswdlogin

adduser administrateur audio
adduser administrateur cdrom
adduser administrateur video
adduser administrateur sudo
adduser administrateur netdev
adduser administrateur plugdev
adduser administrateur fuse
adduser administrateur users
adduser administrateur lp
adduser administrateur lpadmin
adduser administrateur scanner
adduser 01-mini audio
adduser 01-mini cdrom
adduser 01-mini video
adduser 01-mini netdev
adduser 01-mini plugdev
adduser 01-mini fuse
adduser 01-mini users
adduser 01-mini lp
adduser 01-mini scanner
adduser 02-super audio
adduser 02-super cdrom
adduser 02-super video
adduser 02-super netdev
adduser 02-super plugdev
adduser 02-super fuse
adduser 02-super users
adduser 02-super lp
adduser 02-super scanner
adduser 03-maxi audio
adduser 03-maxi cdrom
adduser 03-maxi video
adduser 03-maxi netdev
adduser 03-maxi plugdev
adduser 03-maxi fuse
adduser 03-maxi users
adduser 03-maxi lp
adduser 03-maxi scanner
chown -R administrateur:administrateur /home/01-mini/.config/lxpanel
chown -R administrateur:administrateur /home/01-mini/.config/xfce4
chown -R administrateur:administrateur /home/02-super/.config/lxpanel
chown -R administrateur:administrateur /home/02-super/.config/xfce4
chown -R administrateur:administrateur /home/03-maxi/.config/lxpanel
chown -R administrateur:administrateur /home/03-maxi/.config/xfce4
chown -R administrateur:administrateur /home/01-mini/.config/libfm
chown -R administrateur:administrateur /home/02-super/.config/libfm
chown -R administrateur:administrateur /home/03-maxi/.config/libfm
chown -R administrateur:administrateur /home/01-mini/.fluxbox/startup
chown -R administrateur:administrateur /home/02-super/.fluxbox/startup
chown -R administrateur:administrateur /home/03-maxi/.fluxbox/startup
chown administrateur:administrateur /home/01-mini/.config/rox.sourceforge.net/ROX-Filer/Options
chown administrateur:administrateur /home/02-super/.config/rox.sourceforge.net/ROX-Filer/Options
chown administrateur:administrateur /home/03-maxi/.config/rox.sourceforge.net/ROX-Filer/Options
chown administrateur:administrateur /home/01-mini/.config/rox.sourceforge.net/ROX-Filer/globicons
chown administrateur:administrateur /home/02-super/.config/rox.sourceforge.net/ROX-Filer/globicons
chown administrateur:administrateur /home/03-maxi/.config/rox.sourceforge.net/ROX-Filer/globicons
chown administrateur:administrateur /home/01-mini/.config/rox.sourceforge.net/ROX-Filer/menus2
chown administrateur:administrateur /home/02-super/.config/rox.sourceforge.net/ROX-Filer/menus2
chown administrateur:administrateur /home/03-maxi/.config/rox.sourceforge.net/ROX-Filer/menus2
chown administrateur:administrateur /home/01-mini/.config/rox.sourceforge.net/ROX-Filer/panels
chown administrateur:administrateur /home/02-super/.config/rox.sourceforge.net/ROX-Filer/panels
chown administrateur:administrateur /home/03-maxi/.config/rox.sourceforge.net/ROX-Filer/panels
chmod -R 777 /home/02-super/abuledu-primtux/abuledu-aller
chmod -R 777 /home/03-maxi/abuledu-primtux/abuledu-aller
chown -R 01-mini:01-mini /home/01-mini/.wine
chown -R 02-super:02-super /home/02-super/.wine
chown -R 03-maxi:03-maxi /home/03-maxi/.wine
chsh -s /bin/inactif 01-mini;
chsh -s /bin/inactif 02-super;
chsh -s /bin/inactif 03-maxi;
chsh -s /bin/bash administrateur;
cd /home/01-mini
ln -s ../administrateur/Public Public
cd /home/02-super
ln -s ../administrateur/Public Public
cd /home/03-maxi
ln -s ../administrateur/Public Public
chmod -R 2777 /home/administrateur/Public

# Ajout du support de l'architecture i386
dpkg --add-architecture i386
apt-get update

# Installe les paquets i386
for paquet in ${paquets386}
do
  apt-get install -y "$paquet"
done

# On tente la réinstallation des paquets dont l'installation a échoué
ct=1
while [ $ct -le $tentatives ]; do
  etablit_manquants
  if [ $erreurs = "oui" ]; then
    for paquet in ${manquants}; do
      apt-get install -y "$paquet"
    done
  fi
  let "ct += 1"
done
etablit_manquants
reponse="oui"
if [ "$erreurs" = "oui" ]; then
  while [ "$reponse" = "oui" ]; do
    echo ""
    echo "Les paquets suivants n'ont pas pu être installés malgré $tentatives tentatives :"
    echo "$manquants"
    echo "Voulez-vous faire une nouvelle tentative ? O/N (par defaut O)"
    read choix
    case "$choix" in
        [Oo]) reponse="oui" ;;
        [Nn]) reponse="non" ;;
        *) reponse="oui"
    esac
    if [ "$reponse" = "oui" ]; then
      for paquet in ${manquants}; do
         apt-get install -y "$paquet"
      done
      let "tentatives += 1"
    fi
    etablit_manquants
    if [ "$erreurs" = "non" ]; then
      break
    fi
  done
fi

# Installe les paquets locaux
apt-get install -y "$paquets_locaux"

# Indication de version de l'OS
echo "$version" >/etc/primtux_version

# Nettoyage
rm /var/cache/apt/archives/*.deb
rm /etc/apt/sources.list.d/artful.list

# On génère les noyaux
if [ -z $(find /boot -name "vmlinuz*") ]
  then apt-get install --reinstall $(dpkg -l | grep 'linux-image-[0-9]' | tr -s ' ' | cut -d ' ' -f 2)
fi
update-initramfs -k all -u

# Vérifie la bonne installation des paquets locaux
dpkg -l | sed -e '1,6d' -e "s/[ ][ ]*/#/g" | cut -d '#' -f 2 > "$installes"
while read paquet; do
  if ! grep "$paquet" "$installes" > /dev/null
      then manquants="$manquants $paquet"
           erreurs="oui"
    fi
done <<<"$liste_locaux"

# Si des paquets n'ont malgré tout pu être installés, on écrit lesquels dans le fichier log
if [ "$erreurs" = "oui" ]; then
  echo "
++++++++++++++++++++++++++++++++++++++++++++++++
Les paquets suivants n'ont pas pu être installés
++++++++++++++++++++++++++++++++++++++++++++++++
$manquants
" >> "$fic_log"
  echo "$manquants" >"$fic_liste_manquants"
fi

rm "$installes"

exit 0
