# Construction d'une PrimTux sur une base Lubuntu #

Ce script a été réalisé grâce au tutoriel de Stéphane Deudon : [Compile ta PrimTux Lubuntu](https://wiki.primtux.fr/lib/exe/fetch.php/documents:compile-ta-primtux-lubuntu.pdf).

Après avoir téléchargé une Lubuntu, indiquer dans la variable `chemin_iso`, au début du script `ptx-lubuntu.sh`, le chemin d'accès au fichier, ainsi que la la version de PrimTux dansla variable $version en début de script operations-chroot.sh.

Les deux scripts doivent être placés dans le même répertoire.
On lance un terminal dans le répertoire des scripts, et on lance la construction par

    sudo ./ptx-lubuntu.sh
ou

    sudo bash ptx-lubuntu.sh

Toutes les opérations sont automatisées, mais il sera nécessaire d'intervenir 4 fois pour des configurations demandées par certains paquets à installer :

- deux fois pour les règles ip V4 et ip V6
- une fois pour le codage du clavier
- une fois pour une demande de mot de passe localhost qu'on peut laisser vide.

Si certains paquets n'ont pu être installés, le script tentera 3 fois de suite une nouvelle installation. Le nombre de tentatives à effectuer peut être changé au début du script `operations-chroot.sh`.

Si des paquets n'ont toujours pas pu être installés, le script demande si l'on veut faire une nouvelle tentative. Tant que l'utilisateur ne répond pas non, le script réssaiera les installations. Cela peut-être utile en cas de difficultés à se connecter aux dépôts, ou en cas de difficultés de connexion Internet. On pourra également intervenir en chroot sur le système depuis un autre terminal afin de résoudre d'éventuels problèmes avant de relancer l'installation des paquets manquants.

La liste des paquets à installer peut-être aisément modifiée au début du script `operations-chroot.sh` au sein des variables `paquets`, `paquets386` et `paquets_locaux`.

Avant la création de l'iso, le script demande si l'on souhaite ou non supprimer le répertoire de construction du système. Cela peut être utile sur une machine virtuelle avec un espace disque limité, le répertoire de travail étant très volumineux.

Le script crée un dossier `iso`, non supprimé, dans le répertoire de l'utilisateur. Il contient tout ce qui est nécessaire pour reconstruire l'iso avec xorriso. Il pourra être effacé s'il ne présente plus d'utilité.
